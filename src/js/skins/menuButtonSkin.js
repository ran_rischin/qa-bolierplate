define({
    type: 'button',
    version: '1.0',
    id: 'menuButton',
    html: '<div data-ref="main" style="box-sizing:border-box;display:block;width:100%;height:100%;"><div data-ref="container" style="position:relative;width:100%;height:100%;background-color:rgba(255,255,255,0.5);border:1px solid #ffffff;">&nbsp;</div></div>',

    onStateChanged: function(newState, oldState) {
        'use strict';

        var Power1 = Power1 || {};

        switch (newState){

            case 'created': {
                this.animate(this.ui.container, 0, { opacity: 0 });
                break;
            }

            case 'idle': {
                this.animate(this.ui.container, 0.25, { opacity: 0, ease: Power1.easeInOut });
                break;
            }

            case 'idleover': {
                this.animate(this.ui.container, 0.25, { opacity:0.5, ease: Power1.easeInOut });
                break;
            }

            case 'selected': {

                this.animate(this.ui.container, 0.25, { opacity:0.8, ease: Power1.easeInOut });
                break;
            }

            case 'destructed': {
                this.animate(this.ui.container, 0.25, { opacity: 0, ease: Power1.easeInOut });
                break;
            }
        }
        return true;
    }
});
