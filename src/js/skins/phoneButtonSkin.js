define({
    type: 'button',
    version: '1.0',
    id: 'phoneButton',

    options: {
        appearDelay: 0,
        buttonHovered: false,
        svgImage: ''
    },

    html: '<div data-ref="main" style="box-sizing:border-box;display:block;"><div data-ref="container" style="position:relative;width:100%;height:100%;"><div data-ref="iconContainer" style="position:relative;width:100%;height:100%;"><div data-ref="icon" style="fill:white;position:relative;z-index:2;">{{{svgImage}}}</div></div><div data-ref="background" style="position:absolute;width:100%;height:100%;z-index:1;background-color:rgb(255,255,255);top:0;left:0;">&nbsp;</div></div> <div data-ref="hit" style="position:absolute;width:100%;height:100%;z-index:999;top:0;">&nbsp;</div></div>',

    onStateChanged: function (newState, oldState) {
        'use strict';

        var Power1 = Power1 || {};
        var Power3 = Power3 || {};

        switch (newState) {

            case 'created': {
                this.animate(this.ui.icon, 0, { scale: 0.5, opacity: 0 });
                this.animate(this.ui.background, 0, { opacity: 0 });
                break;
            }

            case 'idle': {
                if (this.options.buttonHovered === false) {
                    this.animate(this.ui.icon, 0.35, { startAt: { scale: 0.5, opacity: 0 }, scale: 1, opacity: 1, ease: Power3.easeOut, delay: this.options.appearDelay });
                }
                else {
                    this.options.buttonHovered = false;
                }
                break;
            }

            case 'idleover': {
                this.options.buttonHovered = true;
                this.animate(this.ui.icon, 0.3, { startAt: { scale: 1 }, scale: 1.05, ease: Power1.easeInOut, repeat: -1, yoyo: true, onRepeatParams: ['{self}'], onRepeat: function (e) {
                    if (e._cycle % 2 === 0) {
                        if (this.options.buttonHovered === false) {
                            e.kill();
                        }
                    }
                }});

                break;
            }

            case 'selected': {
                this.options.buttonHovered = true;
                this.animate(this.ui.background, 0.3, { opacity: 0.3, ease: Power3.easeIn });
                this.animate(this.ui.icon, 0.3, { startAt: { scale: 1 }, scale: 1.05, ease: Power1.easeInOut, repeat: -1, yoyo: true, onRepeatParams: ['{self}'], onRepeat: function (e) {
                    if (e._cycle % 2 === 0) {
                        if (this.options.buttonHovered === false) {
                            e.kill();
                        }
                    }
                }});
                break;
            }

            case 'inactive': {
                this.animate(this.ui.icon, 0.3, { opacity: 0.4, ease: Power3.easeIn });
                break;
            }

            case 'destructed': {
                this.options.buttonHovered = false;
                this.animate(this.ui.icon, 0.3, { scale: 0.8, opacity: 0, ease: Power3.easeIn });
                this.animate(this.ui.background, 0.3, { opacity: 0, ease: Power3.easeIn });
                break;
            }
        }
        return true;
    }
});
