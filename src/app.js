define([
    'resources/ivds',
    'resources/nodes',
    'text!config/settings.json',
    'text!config/autopilot.json',
    'text!resources/skins.json',
    'resources/overlays'
],
function(ivds, nodes, settings, autopilot, skins, overlays) {

    'use strict';

    // app.js variables

    // hold a reference to the Interlude Player
    var player;
    var interlude;

    function onNodeStart() {
        console.log('node has started');
    }

    //
    // project essentials
    //
    return {
        // the fullowing are expected to be provided for Helix
        settings: JSON.parse(settings),

        // Will be called by Helix and is responsible to preare the player
        // for playback of the app (e.g. adding nodes, GUI, etc')
        onInit: function(interludeRef) {
            player = interludeRef.player;
            interlude = interludeRef;

            // Project structure.
            // player.project.addNodes(nodes, ivds);
            // player.project.buildGraphAndDecision(nodes);
            // GUI
            // Add parallelIndication
            player.gui.addSkin(
                { id: 'defaultIndication', url: 'https://d1w2zhnqcy4l8f.cloudfront.net/treehouseResources/parallel_indications/defaultParallelIndication.iskn'}
                );
            player.gui.add(player.uitools.React.DOM.div({ id: 'parallelIndication', zIndex: 3, scaling: false, boxing: false,
                style: { width: '100%', height: '100%', visibility: 'visible' }},
                            player.gui.InterludeParallelIndication({
                                id: 'parallelIndication',
                                skinSource: 'defaultIndication'
                            })
                        )
            );
            player.gui.addSkin(JSON.parse(skins));
            player.gui.add(overlays.overlays);

            /* player.repository.add({
                id: 'demo::intro',
                source: ivds.A,
            });*/
        },

        // the ID for the head node to be loaded by Helix. This can also be
        // a function.
        // head: 'demo::intro',

        // Will be called automatically at the start of the headnode.
        onStarted: function() { // active user click , autoplay , continous need to get that from helix
            console.log('app onStarted');
            // report view so Amit will be bale to get some sleep tonight
            if (player.analytics !== undefined) {
                // set project ID - need to be set by the helix / cluster ??? curretn set by the loader
                // proj.player.analytics.setGlobalProperty(projectID, settings.id);
                // increase projectInSession each time project started
                player.analytics.setGlobalProperty(
                    'projectInSession',
                    (player.analytics.getGlobalProperty('projectInSession') || 0) + 1
                );
                // track view
                player.analytics.view('active');
            }
            // player.on('nodestart', onNodeStart, 'demo');
            player.on(player.events.guiclick, this.interaction, 'demo');
        },

        // Will be called by Helix once the project is about to reach its end.
        // Responsible for cleanup.
        onEnd: function(completed) {
            if (player.analytics !== undefined) {
                var type = (completed) ? 'completed' : 'interrupted';
                player.analytics.viewEnd(type);
            }
            // do something...
            player.off(null, null, 'demo');
        },

        // track interactin when button clicked
        interaction: function(e) {
            if (player.analytics !== undefined) {
                if (e.context && e.context.props && e.context.props.behavior && typeof e.context.props.behavior === 'object') {
                    var context = e.context;
                    var props = context.props;
                    var behavior = props.behavior;
                    var interactionId = player.currentNode.id;
                    var actionName = 'decision';

                    console.log(context.parentComp.getAttribute('id'));
                    if (context.parentComp && context.parentComp.getAttribute('id') === 'menu') {
                        interactionId = 'persistent';
                    }
                    if (behavior.type === 'selectnode' && behavior.hasOwnProperty('connectOn') && behavior.connectOn === 'click') {
                        actionName = 'jump';
                    }

                    if (behavior.type === 'switchchannel') {
                        actionName = 'channelswap';
                    }

                    player.analytics.interaction(interactionId, props.id, { trigger: 'click', action: actionName });
                }
            }
            console.log(e);
        }
    };

});
