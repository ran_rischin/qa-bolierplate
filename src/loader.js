require.config({
    paths: {
        'text': 'https://cdnjs.cloudflare.com/ajax/libs/require-text/2.0.12/text.min',
        'InterludePlayer': 'https://d3425luerwqydx.cloudfront.net/players/htmlplayer/release/3.0.6/4/player.min.gz',
        'InterludePlayerPlugins': 'https://d3425luerwqydx.cloudfront.net/players/htmlplayer/release/3.0.6/4/plugins.min.gz',
        //ran local player
        /*'InterludePlayer': 'http://htmlplayer.interlude-dev003.fm/dist/player.min',
        'InterludePlayerPlugins': 'http://htmlplayer.interlude-dev003.fm/dist/plugins.min',*/
        'when': 'https://cdnjs.cloudflare.com/ajax/libs/when/2.7.1/when'
    },
    shim: {
        'InterludePlayerPlugins': {
            deps: ['InterludePlayer']
        }
    },
    waitSeconds: 100
});

require([
    'when',
    'InterludePlayer',
    'InterludePlayerPlugins',
    'app.min',
    'text!resources/config.js'
],
function(when, InterludePlayer, InterludePlayerPlugins, app, config) {
    'use strict';

    function checkEnvironment() {
        var env = InterludePlayer.environment;
        var osVersion = env.os.version.split('.');
        var osVerMaj = parseInt(osVersion[0]);
        var osVerMin = parseInt(osVersion[1]);
        var guiSupport = true;
        var isIE = false;
        var isIphone = (env.os.name === 'iOS' && env.device.model === 'iPhone');
        var header = 'NO CHROME, NO PARTY';
        var message = 'Get Chrome to play this interactive video in full glory.';
        var downloadLink = 'https://play.google.com/store/apps/details?id=com.android.chrome';
        // var redirectLink = 'interlude://video?html_url=http://ceelo.in.fm/cluster/v1/world/cluster/dev/dist.html';
        var isFacebookApp = false;
        var oldIos = false;
        var deviceSupported = InterludePlayer.isDeviceSupported();

        // This is shown if all else fails
        if (!deviceSupported && !isIphone) {
            header = 'OOPS';
            message = 'Well this is awkward. We don\'t support this device.';
        }
        // Check if we're on iPhone
        else if (isIphone) {
            isIphone = true;
            if ((osVerMaj < 8) && !(osVerMaj === 7 && osVerMin === 1)) {
                oldIos = true;
                header = 'PLEASE UPGRADE';
                message = 'You need to have iOS 7.1 and above to enjoy the wonders of the Interlude app on iPhone. Go on. Treat yourself.';

            } else {

                header = 'NO APP, NO PARTY';
                message = 'Download the Interlude app to play this interactive video in full glory.';
                downloadLink = 'https://itunes.apple.com/il/app/interlude-player/id654456835';
                if (
                    (
                        navigator.userAgent.indexOf('FBAN/') > -1 &&
                        navigator.userAgent.indexOf('FBAV/') > -1 &&
                        navigator.userAgent.indexOf('FBBV/') > -1
                    ) ||
                    (
                        navigator.userAgent.indexOf('Twitter for iPhone') > -1
                    )
                ) {
                    isFacebookApp = true;
                }


            }

        }

        // check if we're on IE < 11
        if (env.os.name === 'Windows' && env.browser.name === 'IE' && parseInt(env.browser.major) < 11) {
            isIE = true;
            guiSupport = false;
            header = 'PLEASE UPGRADE';
            message = 'Upgrade to IE 11 to play this interactive video in full glory.';
            downloadLink = 'http://windows.microsoft.com/en-us/internet-explorer/download-ie';
        }

        // If player is supported on current device/environment, start cluster
        if (InterludePlayer.isSupported() && guiSupport) {
            return true;
        }

        // Otherwise, if player is not supported in current browser but IS supported in device, show "download Chrome" screen
        else {
            document.body.innerHTML = '' +
                '<div id="notSupportedErrorscreen" style="position:absolute;background-color:rgba(26, 31, 40, 0.8);background-image:url(https://d1isb7w5npkwv3.cloudfront.net/ceelo/ceeloBack.jpg);background-size:cover;background-position:50% 50%;color:#FFF;width:100%;height:100%;pointer-events:auto;z-index:200000;">' +
                    '<div style="text-align:center;bottom:0;position:absolute;width:calc(100% - 50px);font-family:MuseoSans,sans-serif;font-weight:400;font-size:16px;line-height:20px;color:#FFF;padding:25px;">' +
                        '<div id="notSupportedTitle" style="padding-top:25px;color:#ffffff;font-size:26px;line-height:30px;font-weight:900;letter-spacing:2px;">' +
                            '<span>' + header + '</span>' +
                        '</div>' +
                        '<div style="padding-top:8px;font-weight:100;">' +
                            '<span>' + message + '</span>' +
                        '</div>' +
                        (((!deviceSupported && !isIphone) || ((isIphone && !isFacebookApp) || oldIos)) ? '' : ('<a id="notSupportedDownloadButton" href="' +  downloadLink + '" target="_blank" style="border-radius: 2px; color: rgb(255, 255, 255); font-family:MuseoSans,sans-serif; text-decoration:none; font-size: 26px; font-weight:bold;display:inline-block; text-align: center; line-height: 66px; margin-top: 25px; cursor: pointer; width: 100%; max-width:450px; background-color: #e73e50;">DOWNLOAD</a>')) +
                        ((isIphone && !oldIos) ? ('<a id="notSupportedWatchButton" style="border-radius: 2px; color: rgb(255, 255, 255); font-family:MuseoSans,sans-serif; text-decoration:none; font-size: 26px; font-weight:bold;display:inline-block; text-align: center; line-height: 66px; margin-top: 25px; cursor: pointer; width: 100%; max-width:450px; background-color: #e73e50;">WATCH IN APP</a>') : '') +
                    '</div>' +
                '</div>"';
            if (isIphone) {
                document.querySelector('#notSupportedWatchButton').addEventListener('click', playOnIphone);
            }
            return false;
        }

    }

    function playOnIphone() {
        var now = new Date().valueOf();
        setTimeout(function() {
            var timeout = new Date().valueOf();
            //console.log(timeout);
            //console.log(now);

            if (timeout - now > 50) {
                // probably means the app was opened and the browser was closed
                //app is installed.
                return;
            }
            //app is not installed.
            window.parent.parent.location.href = "https://itunes.apple.com/il/app/interlude-player/id654456835";
        }, 25);

        // If "custom-uri://" is registered the app will launch immediately and your
        // timer won't fire. If it's not set, you'll get an ugly "Cannot Open Page"
        // dialogue prior to the reloading the page with ignoreautoload param

        // injects openAppURL into the page (the URL without the scheme part)
        var openAppURL = "video?html_url={{embedUrl}}&thumb={{ogImage}}&title={{iosTitle}}";
        window.location = "interlude://" + openAppURL;
    }

    if (!checkEnvironment()) {
        return;
    }

    var interlude = {};
    window.debug = true;
    var mode = (window.debug) ? 'qa' : 'production';

    function onStarted() {
        console.log('loader onStarted');
        app.onStarted();
        // addlisteners
        //interlude.player.off('nodestart', onStarted);
        interlude.player.on(interlude.player.events.ended, onEnd);
        interlude.player.on('seek.stop', onSeekStop);

    }

    function onEnd() {
        app.onEnd(true);
        interlude.player.once('nodestart', onStarted);
        interlude.player.off(interlude.player.events.ended, onEnd);
        interlude.player.off('seek.stop', onSeekStop);
    }

    function onSeekStop() {
        app.onEnd();
        interlude.player.once('nodestart', onStarted);
        interlude.player.off(interlude.player.events.ended, onEnd);
        interlude.player.off('seek.stop', onSeekStop);
    }
    // create a player and add it to the "interlude" global anchor
    interlude.player = window.player = new InterludePlayer('#player',
    {
        Controller: {
            forceBaseUrl: {
                cf: '//d1w2zhnqcy4l8f.cloudfront.net',
                fastly: '//storageinterludefm.global.ssl.fastly.net'
            },
            measureDownloadFile: '/player_measure_bandwidth/measure_bandwidth_download_file.mp4'
        },
        plugins: {
            audio: {},
            graph: {},
            decision: {},
            time: {},
            seek: {},
            urls: {},
            uitools: {},
            gui: {
                triggerButtonEvents: true
            },
            overlays: {},
            project:{
                config: JSON.parse(config)
            },
            controlbarTreehouse: {
                // need to inject the project details name , url and etc
            },
            analytics: {
                debug: true,
                Tracker: {
                    mode: mode
                },
                Mixpanel: {
                    mode: mode
                },
                // global props
                globalProps: {
                    context: 'standalone',
                    projectID: '{{projectId}}'
                }
            },
            /* projectAnalytics: {},*/
            sentry: {
                mode: mode
            },
        }
    });

    // when the gui is ready, add the components
    interlude.player.gui.promise.done(function() {

        // Start
        if (window.debug) {
            // addListeners();
            player.initPlugin('debug', {});
        }
        var d = when.defer();
        app.onInit(interlude, function (err) {
            if (err) {
                // TODO: what should we do in this case?
                console.log('project initialization failed.');
                throw 'project onInit() failed';
            }
            d.resolve();
        });

        // synchronous init can resolve now
        if (!app.settings.asyncInit) {
            d.resolve();
        }
        d.promise.then(function () {
            //interlude.player.once('nodestart', app.onStarted);
            interlude.player.once('nodestart', onStarted);

            // interlude.player.append((typeof app.head === 'function') ? app.head() : app.head);

            // interlude.player.initPlugin('controlbarTreehouse', {});

            if (app.settings.autoPlay) {
                interlude.player.play();
            }
        });

    });

    if (interlude.player.videoTech() === 'hls') {

        /*** refresh page if tab out of focus on hls engine ***/
        var getHiddenProp = function() {
            var prefixes = ['webkit', 'moz', 'ms', 'o'];

            // if 'hidden' is natively supported just return it
            if ('hidden' in document) {
                return 'hidden';
            }

            // otherwise loop over all the known prefixes until we find one
            for (var i = 0; i < prefixes.length; i++) {
                if ((prefixes[i] + 'Hidden') in document) {
                    return prefixes[i] + 'Hidden';
                }
            }

            // otherwise it's not supported
            return null;                    
        };

        var isHidden = function() {
            var prop = getHiddenProp();
            if (!prop) {
                return false;
            }
            return document[prop];
        };

        var visChange = function() {
            if (!isHidden()) {
                interlude.player.seek.stop();
            }
        };

        var visProp = getHiddenProp();
        if (visProp) {
            var evt = visProp.replace(/[H|h]idden/, '') + 'visibilitychange';
            document.addEventListener(evt, visChange);
        }
    }

}

);
