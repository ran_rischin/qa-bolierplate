{
    "settings": {
        "autoPlay": false,
        "enableSeek": true,
        "projectId": "ec6a6e20-40e3-11e5-8caf-4f09ac474d0c",
        "projectVersion": "",
        "revision": "9",
        "projectName": "html test",
        "creatorName": "Ran Rischin",
        "projectUrl": "https://v.interlude.fm/v/V1Y1GV",
        "embedUrl": "https://v.interlude.fm/embed/V1Y1GV"
    },
    "head": "node_beginning_4jl06k44gkcgg8c0s44wwksw8c0kc",
    "segments": {
        "video_acceptance_2_7kl0v2ul5e4o80wc4k0wwgswg0ws": {
            "metadata": {
                "type": "regular",
                "baseUrl": "https://d1w2zhnqcy4l8f.cloudfront.net"
            },
            "source": [{
                "ts": {
                    "bitrate": {
                        "750": {
                            "url": "/118853421355cc86d1778e4032711437.ts",
                            "byterange": [{
                                "duration": 1,
                                "length": 118064,
                                "offset": 0
                            }, {
                                "duration": 1,
                                "length": 113364,
                                "offset": 118064
                            }, {
                                "duration": 1,
                                "length": 112612,
                                "offset": 231428
                            }, {
                                "duration": 1,
                                "length": 95880,
                                "offset": 344040
                            }, {
                                "duration": 1,
                                "length": 92496,
                                "offset": 439920
                            }, {
                                "duration": 1,
                                "length": 151340,
                                "offset": 532416
                            }, {
                                "duration": 1,
                                "length": 156416,
                                "offset": 683756
                            }, {
                                "duration": 1,
                                "length": 130660,
                                "offset": 840172
                            }, {
                                "duration": 1,
                                "length": 131224,
                                "offset": 970832
                            }, {
                                "duration": 0.28,
                                "length": 54332,
                                "offset": 1102056
                            }],
                            "contentLength": 1156388
                        }
                    },
                    "duration": 9.28
                },
                "mp4": {
                    "bitrate": {
                        "768": {
                            "url": "/118853421355cc86d1778e4032711437.mp4",
                            "contentLength": 1048674
                        }
                    },
                    "duration": 9.311189
                },
                "flv": {
                    "bitrate": {
                        "1500": {
                            "url": "/cbc4ed660462dc621dfbf1223c06d70c.flv",
                            "contentLength": 1900059
                        }
                    },
                    "duration": 9.32
                }
            }]
        },
        "video_acceptance_3_7kp5lqjrnlcsgoog00oo0k848o8o": {
            "metadata": {
                "type": "regular",
                "baseUrl": "https://d1w2zhnqcy4l8f.cloudfront.net"
            },
            "source": [{
                "ts": {
                    "bitrate": {
                        "750": {
                            "url": "/195196920155cc86d158951048737684.ts",
                            "byterange": [{
                                "duration": 1,
                                "length": 120508,
                                "offset": 0
                            }, {
                                "duration": 1,
                                "length": 143820,
                                "offset": 120508
                            }, {
                                "duration": 1,
                                "length": 123704,
                                "offset": 264328
                            }, {
                                "duration": 0.64,
                                "length": 79524,
                                "offset": 388032
                            }],
                            "contentLength": 467556
                        }
                    },
                    "duration": 3.64
                },
                "mp4": {
                    "bitrate": {
                        "753": {
                            "url": "/195196920155cc86d158951048737684.mp4",
                            "contentLength": 407483
                        }
                    },
                    "duration": 3.668756
                },
                "flv": {
                    "bitrate": {
                        "1500": {
                            "url": "/89edde3b67a9a0b6b439e7bd46012024.flv",
                            "contentLength": 777681
                        }
                    },
                    "duration": 3.68
                }
            }]
        },
        "video_4d69816c95aef4b2cafe5da89a447035_1sqrt80z6fgko4s8gwoo0484ckoc0": {
            "metadata": {
                "type": "parallel",
                "baseUrl": "https://d1w2zhnqcy4l8f.cloudfront.net"
            },
            "source": [{
                "ts": {
                    "bitrate": {
                        "750": {
                            "url": "/27221733155cc86d1629112259864741.ts",
                            "byterange": [{
                                "duration": 1,
                                "length": 132352,
                                "offset": 0
                            }, {
                                "duration": 1,
                                "length": 142128,
                                "offset": 132352
                            }, {
                                "duration": 1,
                                "length": 139496,
                                "offset": 274480
                            }, {
                                "duration": 1,
                                "length": 126148,
                                "offset": 413976
                            }, {
                                "duration": 1,
                                "length": 120132,
                                "offset": 540124
                            }, {
                                "duration": 1,
                                "length": 101144,
                                "offset": 660256
                            }, {
                                "duration": 1,
                                "length": 103964,
                                "offset": 761400
                            }, {
                                "duration": 0.16,
                                "length": 26132,
                                "offset": 865364
                            }],
                            "contentLength": 891496
                        }
                    },
                    "duration": 7.16
                },
                "mp4": {
                    "bitrate": {
                        "758": {
                            "url": "/88665520055cc86d4369712920603951.imc#1",
                            "contentLength": 1624070
                        }
                    },
                    "duration": 7.198167
                },
                "flv": {
                    "bitrate": {
                        "1500": {
                            "url": "/9662c3afba7e8f9ab9407d0703d184d3.flv#0",
                            "contentLength": 2919091
                        }
                    },
                    "duration": 7.2
                }
            }, {
                "ts": {
                    "bitrate": {
                        "750": {
                            "url": "/150870617555cc86d16d41a097756014.ts",
                            "byterange": [{
                                "duration": 1,
                                "length": 116372,
                                "offset": 0
                            }, {
                                "duration": 1,
                                "length": 109792,
                                "offset": 116372
                            }, {
                                "duration": 1,
                                "length": 108100,
                                "offset": 226164
                            }, {
                                "duration": 1,
                                "length": 91932,
                                "offset": 334264
                            }, {
                                "duration": 1,
                                "length": 90240,
                                "offset": 426196
                            }, {
                                "duration": 1,
                                "length": 147956,
                                "offset": 516436
                            }, {
                                "duration": 1,
                                "length": 162808,
                                "offset": 664392
                            }, {
                                "duration": 0.24,
                                "length": 70312,
                                "offset": 827200
                            }],
                            "contentLength": 897512
                        }
                    },
                    "duration": 7.24
                },
                "mp4": {
                    "bitrate": {
                        "758": {
                            "url": "/88665520055cc86d4369712920603951.imc#2",
                            "contentLength": 1624070
                        }
                    },
                    "duration": 7.2678
                },
                "flv": {
                    "bitrate": {
                        "1500": {
                            "url": "/9662c3afba7e8f9ab9407d0703d184d3.flv#1",
                            "contentLength": 2919091
                        }
                    },
                    "duration": 7.2
                }
            }]
        },
        "video_roberto_benigni_winning_best_actor_43wwyc2grjy84w0c8woockwss0wok": {
            "metadata": {
                "type": "regular",
                "baseUrl": "https://d1w2zhnqcy4l8f.cloudfront.net"
            },
            "source": [{
                "ts": {
                    "bitrate": {
                        "750": {
                            "url": "/80279767355cc86d18c75d1691063581.ts",
                            "byterange": [{
                                "duration": 1,
                                "length": 146640,
                                "offset": 0
                            }, {
                                "duration": 1,
                                "length": 116372,
                                "offset": 146640
                            }, {
                                "duration": 1,
                                "length": 91932,
                                "offset": 263012
                            }, {
                                "duration": 1,
                                "length": 117312,
                                "offset": 354944
                            }, {
                                "duration": 1,
                                "length": 115808,
                                "offset": 472256
                            }, {
                                "duration": 1,
                                "length": 122200,
                                "offset": 588064
                            }, {
                                "duration": 1,
                                "length": 140436,
                                "offset": 710264
                            }],
                            "contentLength": 850700
                        }
                    },
                    "duration": 7
                },
                "mp4": {
                    "bitrate": {
                        "735": {
                            "url": "/80279767355cc86d18c75d1691063581.mp4",
                            "contentLength": 764298
                        }
                    },
                    "duration": 7.035589
                },
                "flv": {
                    "bitrate": {
                        "1500": {
                            "url": "/bd8be81b57738d9d48d9519e128e10b0.flv",
                            "contentLength": 1405989
                        }
                    },
                    "duration": 7.04
                }
            }]
        },
        "video_cuba_gooding_jr_wins_supporting_actor_1997_oscars_45pn6prk3xmokw4co0okw0okgggs0": {
            "metadata": {
                "type": "regular",
                "baseUrl": "https://d1w2zhnqcy4l8f.cloudfront.net"
            },
            "source": [{
                "ts": {
                    "bitrate": {
                        "750": {
                            "url": "/49512871555cc86d1823824369430741.ts",
                            "byterange": [{
                                "duration": 1,
                                "length": 109416,
                                "offset": 0
                            }, {
                                "duration": 1,
                                "length": 104528,
                                "offset": 109416
                            }, {
                                "duration": 1,
                                "length": 120132,
                                "offset": 213944
                            }, {
                                "duration": 1,
                                "length": 120508,
                                "offset": 334076
                            }, {
                                "duration": 1,
                                "length": 150776,
                                "offset": 454584
                            }, {
                                "duration": 1,
                                "length": 146264,
                                "offset": 605360
                            }, {
                                "duration": 1,
                                "length": 107724,
                                "offset": 751624
                            }, {
                                "duration": 0.24,
                                "length": 40420,
                                "offset": 859348
                            }],
                            "contentLength": 899768
                        }
                    },
                    "duration": 7.24
                },
                "mp4": {
                    "bitrate": {
                        "750": {
                            "url": "/49512871555cc86d1823824369430741.mp4",
                            "contentLength": 802927
                        }
                    },
                    "duration": 7.2678
                },
                "flv": {
                    "bitrate": {
                        "1500": {
                            "url": "/473b53d9d5b750c69adb34c9e154120c.flv",
                            "contentLength": 1468000
                        }
                    },
                    "duration": 7.28
                }
            }]
        }
    },
    "nodes": {
        "node_beginning_4jl06k44gkcgg8c0s44wwksw8c0kc": {
            "source": "video_acceptance_3_7kp5lqjrnlcsgoog00oo0k848o8o",
            "metadata": {
                "name": "Beginning",
                "uid": "ec6f2910-40e3-11e5-8caf-4f09ac474d0c"
            },
            "decision": {
                "startTime": 1
            },
            "channels": [{
                "children": ["node_parallel_1sqg6s1i56sgoccgsgw0kcg8owwwc", "node_beginning_4jl06k44gkcgg8c0s44wwksw8c0kc"],
                "seekForwardTo": ["node_parallel_1sqg6s1i56sgoccgsgw0kcg8owwwc"],
                "defaultNodes": ["node_parallel_1sqg6s1i56sgoccgsgw0kcg8owwwc"]
            }]
        },
        "node_node2_3cxb03k7jiww8o8s4kkw0kgk0g4kw": {
            "source": "video_acceptance_2_7kl0v2ul5e4o80wc4k0wwgswg0ws",
            "metadata": {
                "name": "Node2",
                "uid": "af1db8f0-40e4-11e5-8caf-4f09ac474d0c"
            },
            "decision": {
                "startTime": 0
            },
            "channels": [{
                "children": ["node_parallel_1sqg6s1i56sgoccgsgw0kcg8owwwc", "node_beginning_4jl06k44gkcgg8c0s44wwksw8c0kc"],
                "seekForwardTo": [],
                "defaultNodes": []
            }]
        },
        "node_node3_3d62667q8y80c04gww4g8skwkco8s": {
            "source": "video_cuba_gooding_jr_wins_supporting_actor_1997_oscars_45pn6prk3xmokw4co0okw0okgggs0",
            "metadata": {
                "name": "Node3",
                "uid": "af824720-40e4-11e5-8caf-4f09ac474d0c"
            },
            "decision": {
                "startTime": 1
            },
            "channels": [{
                "children": ["node_beginning_4jl06k44gkcgg8c0s44wwksw8c0kc", "node_parallel_1sqg6s1i56sgoccgsgw0kcg8owwwc"],
                "seekForwardTo": [],
                "defaultNodes": ["node_beginning_4jl06k44gkcgg8c0s44wwksw8c0kc"]
            }]
        },
        "node_roberto_benigni_winning_best_actor_1r8zu9bqc0zokckswsoooswscg8cc": {
            "source": "video_roberto_benigni_winning_best_actor_43wwyc2grjy84w0c8woockwss0wok",
            "metadata": {
                "name": "Roberto Benigni winning Best Actor",
                "uid": "5bdff5f0-4188-11e5-8caf-4f09ac474d0c"
            },
            "decision": {
                "startTime": 1
            },
            "channels": [{
                "children": ["node_node2_3cxb03k7jiww8o8s4kkw0kgk0g4kw", "node_node3_3d62667q8y80c04gww4g8skwkco8s", "node_parallel_1sqg6s1i56sgoccgsgw0kcg8owwwc", "node_beginning_4jl06k44gkcgg8c0s44wwksw8c0kc"],
                "seekForwardTo": ["node_node2_3cxb03k7jiww8o8s4kkw0kgk0g4kw"],
                "defaultNodes": ["node_node2_3cxb03k7jiww8o8s4kkw0kgk0g4kw", "node_node3_3d62667q8y80c04gww4g8skwkco8s"]
            }]
        },
        "node_parallel_1sqg6s1i56sgoccgsgw0kcg8owwwc": {
            "source": "video_4d69816c95aef4b2cafe5da89a447035_1sqrt80z6fgko4s8gwoo0484ckoc0",
            "metadata": {
                "name": "Parallel",
                "uid": "5e19d2e0-40e4-11e5-8caf-4f09ac474d0c",
                "channels": [{
                    "id": "node_node1_1sqkbip7bp28owo4o8wkkgo8c8sgs",
                    "uid": "5e1b3270-40e4-11e5-8caf-4f09ac474d0c"
                }, {
                    "id": "node_not_to_be_1sqn0cr4gvi880o8w8g0w0c8wg8o8",
                    "uid": "5e1c1cd0-40e4-11e5-8caf-4f09ac474d0c"
                }]
            },
            "decision": {
                "startTime": 0.01
            },
            "channels": [{
                "defaultNodes": ["node_roberto_benigni_winning_best_actor_1r8zu9bqc0zokckswsoooswscg8cc"],
                "seekForwardTo": ["node_roberto_benigni_winning_best_actor_1r8zu9bqc0zokckswsoooswscg8cc"],
                "children": ["node_roberto_benigni_winning_best_actor_1r8zu9bqc0zokckswsoooswscg8cc"]
            }, {
                "defaultNodes": ["node_roberto_benigni_winning_best_actor_1r8zu9bqc0zokckswsoooswscg8cc"],
                "seekForwardTo": ["node_roberto_benigni_winning_best_actor_1r8zu9bqc0zokckswsoooswscg8cc"],
                "children": ["node_roberto_benigni_winning_best_actor_1r8zu9bqc0zokckswsoooswscg8cc"]
            }]
        }
    }
}
