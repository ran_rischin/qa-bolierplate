define([], function() {
    return {
                playerOptions: {
                    EngineTech: {},
                    plugins: {
                        audio: {},
                        graph: {},
                        decision: {},
                        project: {},
                        time: {},
                        seek: {},
                        urls: {},
                        uitools: {},
                        gui: {},
                        overlays: {},
                        controlbar: {},
                        debug: {}
                    }
                }
            };
});
