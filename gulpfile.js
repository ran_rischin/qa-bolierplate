var fs = require('fs');
var path = require('path');
var request = require('request');
var source = require('vinyl-source-stream');
var rjs = require('requirejs');
var gulp = require('gulp-help')(require('gulp'), {
    hideEmpty: true,
    hideDepsMessage: true
});
var colors = require('colors'); // jshint ignore:line
var del = require('del');
var map = require('map-stream');
var AWS = require('aws-sdk');
var uuid = require('uuid');
var mime = require('mime');
var _ = require('underscore');
var exec = require('child_process').exec;
var gutil = require('gulp-util');
// var gulpif = require('gulp-if')

// Load all gulp plugins automatically and attach
// them to the `plugins` object
var plugins = require('gulp-load-plugins')({
    rename: {
        'gulp-compile-handlebars': 'handlebars'
    }
});

// Temporary solution until gulp 4  https://github.com/gulpjs/gulp/issues/355
var runSequence = require('run-sequence');

var minimist = require('minimist');
var pkg = require('./package.json');
var dirs = pkg['boilerplate-configs'].directories;

var cmdOptions = minimist(process.argv.slice(2), {
    default: {
        type: 'patch',
        version: null,
        sourceUrl: ''
    }
});

var overwritingExistingSubfolder;

var UGLIFY                  = !cmdOptions.nouglify;


var REQUIRE_BASE_URL = 'src';

var IVDS_PATH = 'resources/ivd';
var IVDS_FILE = 'resources/ivds.js';
var IVDS_INDEX_FILE = IVDS_PATH + '/ivdsIndex';
var NODES_FILE = 'resources/nodes.js';
var SKINS_PATH = 'resources/skins';
var SKINS_FILE = 'resources/skins.js';
var SKINS_INDEX_FILE = SKINS_PATH + '/skinsIndex';
var CLEAN_CONFIG_JS = 'config.clean.js';
var CLEAN_CONFIG_JS_FULL_PATH = dirs.import + '/' + CLEAN_CONFIG_JS;
var SOURCE_TO_S3 = [dirs.dist + '/**/*', '!' + dirs.dist + '/debug.html', '!' + dirs.dist + '/app.min.gz.js'];
var APP_ZIP_SOURCE_TO_S3 = [dirs.dist + '/app.min.gz.js'];
var NO_PROJECT_ID = '{{UNIQUE_PROJECT_ID}}';
var NO_FOLDER_NAME = '{{UNIQUE_FOLDER_NAME}}';
var BASE_BOILERPLATE_REPO = /interludedevs\/boilerplate.git/;
var REPO_NAME_REGEX = /([^\/]+)\.git/;

var resourcesObj = {};
var projSettings = {};
var treehouseConf = {};

function createIvdDepObj(file, cb) {
    var key = path.join(path.dirname(path.relative(path.join(REQUIRE_BASE_URL, IVDS_PATH), file.path)), path.basename(file.path, path.extname(file.path))).replace(/\\/g, '/');
    resourcesObj[key] = key;
    cb();
}

function createSkinsDepObj(file, cb) {
    resourcesObj[path.basename(file.path, '.js')] = path.basename(file.path, '.js');
    cb();
}

function mkdirSync(path) {
    try {
        fs.mkdirSync(path);
    } catch (e) {
        if (e.code !== 'EEXIST') {
            throw e;
        }
    }
}

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
    };
}

String.prototype.replaceAll = function(str1, str2) {
    return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|<>\-\&])/g, '\\$&'), 'g'), (typeof(str2) === 'string') ? str2.replace(/\$/g, '$$$$') : str2);
};

function padInt(i) {
    var str = '' + i;
    var pad = '0';
    return pad.substring(0, pad.length - str.length) + str;
}

function calcDateAndTime() {
    var timestamp = Date.now();
    var date = new Date(timestamp);
    var datevalues = [
        date.getFullYear(),
        padInt(date.getMonth() + 1),
        padInt(date.getDate()),
        padInt(date.getHours()),
        padInt(date.getMinutes()),
        padInt(date.getSeconds())
    ];
    return datevalues[0] + '-' + datevalues[1] + '-' + datevalues[2] + ' ' + datevalues[3] + ':' + datevalues[4] + ':' + datevalues[5];
}

function showError(str) {
    console.log(str.red);
}

function showInfo(str) {
    console.log(str.cyan);
}

function fixedEncodeURIComponent (str) {
    return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
        return '%' + c.charCodeAt(0).toString(16);
    });
}

function getFolderName() {
    return projSettings.folderName;
}

function getSubFolderName() {
    return getFolderName() + '/archive/' + projSettings.projectVersion;
}

// takes a url in the format "path/file.jpg?base64" and replaces it with a base64 version
function urlToBase64(path) {
    var imagePath = dirs.src + '/' + path;
    var imageBuffer = fs.readFileSync(imagePath);
    var mimeType = mime.lookup(imagePath);

    var ret = 'data:';
    ret += mimeType;
    ret += ';base64,';
    ret += imageBuffer.toString('base64');
    return ret;
}

var allKeys = [];
var keysForDeletion = [];
var pathsForInvalidation = [];

var s3Config = pkg['boilerplate-configs'].s3;
var s3plugin = plugins.s3Upload({
    accessKeyId: s3Config.accessKeyId,
    secretAccessKey: s3Config.secretAccessKey
});
var S3 = new AWS.S3({
    accessKeyId: s3Config.accessKeyId,
    secretAccessKey: s3Config.secretAccessKey
});
var cloudfront = new AWS.CloudFront({
    accessKeyId: s3Config.accessKeyId,
    secretAccessKey: s3Config.secretAccessKey,
    apiVersion: '2015-04-17'
});
var cloudfrontConfig = pkg['boilerplate-configs'].cloudfront;


// ---------------------------------------------------------------------
// | Helper tasks                                                      |
// ---------------------------------------------------------------------


gulp.task('clean', 'deletes dist folder', function(done) {
    del([dirs.archive, dirs.dist], done);
});

gulp.task('copy', false, ['create-project-settings-object'], function(done) {
    runSequence(
        'copy:loader.js',
        'copy:index.html',
        'copy:embed.html',
        'copy:oembed.json',
        'copy:oembed.xml',
        'copy:publicObject.json',
        'copy:debug.html',
        'copy:misc',
    done);
});

gulp.task('remove-comments', false, function () {
    return gulp.src(dirs.dist + '/index.html')
               .pipe(plugins.replace(/<\!\-\-(.|\n)*?\-\->/g, ''))
        .pipe(gulp.dest(dirs.dist));
});



gulp.task('copy:misc', false, function () {
    return gulp.src([
        // Copy all files
        dirs.src + '/**/*',

        // Exclude the following files
        // (other tasks will handle the copying of these files)
        '!' + dirs.src + '/loader.js'
    ], {

        // Include hidden files by default
        dot: true

    }).pipe(gulp.dest(dirs.dist));
});

gulp.task('lint:js', false, function () {
    return gulp.src([
        'gulpfile.js',
        dirs.src + '/js/*.js'
    ]).pipe(plugins.jscs())
      .pipe(plugins.jshint())
      .pipe(plugins.jshint.reporter('jshint-stylish'))
      .pipe(plugins.jshint.reporter('fail'));
});

gulp.task('start-webserver', false, function() {
    // var folder = cmdOptions.src ? dirs.src : dirs.dist;
    var pathToServe = (cmdOptions.debug) ? 'http://localhost:8000/debug.html' : true;
    var folder = dirs.dist;
    var reload = !cmdOptions.noreload;
    showInfo('Serving folder: ' + folder);

    return gulp.src(folder)
        .pipe(plugins.webserver({
            livereload: reload, // You might need the livereload extension for chrome to work
            open: pathToServe
        }));
});



gulp.task('bump', false, function() {
    return gulp.src(dirs.src + '/config/settings.json')
      .pipe(plugins.bump({
          type: cmdOptions.type,
          version: cmdOptions.version,
          key: 'projectVersion'
      }))
      .pipe(gulp.dest(dirs.src + '/config/'));
});

function doesFolderExist(done) {
    S3.listObjects(
        {
            Bucket: s3Config.bucket,
            Marker: '',
            Prefix: getSubFolderName()
        }, function(err, data) {
            if (data.Contents.length > 0) {
                overwritingExistingSubfolder = true;
                done();
            }
            else {
                overwritingExistingSubfolder = false;
                done();
            }
        });
}

gulp.task('generate-project-id', false, function(cb) {

    exec('git remote -v show', function (err, stdout) {

        // on error return
        if (err) { cb(err); return; }

        // check if this is the base boilerplate
        // and get the repo name - to be used for folder name
        var isBaseBoilerplate = stdout.match(BASE_BOILERPLATE_REPO);
        var folderName = stdout.match(REPO_NAME_REGEX)[1];
        if (isBaseBoilerplate) { cb(); return; }

        // update the settings file with the projectId and folderName
        var settingsFileName = dirs.src + '/config/settings.json';
        var settingsContents = fs.readFileSync(settingsFileName).toString();
        if (settingsContents.indexOf(NO_PROJECT_ID) >= 0) {

            // replace projectId and folderName
            settingsContents = settingsContents
                .replace(NO_PROJECT_ID, uuid.v1())
                .replace(NO_FOLDER_NAME, folderName);

            // check for folder name conflict
            projSettings.folderName = folderName;
            doesFolderExist(function() {
                if (overwritingExistingSubfolder) {
                    throw 'ERROR: your repository name matches an existing S3 directory';
                }
                fs.writeFileSync(settingsFileName, settingsContents);
                console.log('boilerplate generated a new project ID and folder name in your settings file'.underline.blue);
                cb();
            });
            return;
        }

        cb();
    });
});


function parseJson(source, target, callback) {
    return gulp.src(source)
        .pipe(plugins.tap(function(file) {
          _.extend(target, JSON.parse(String(file.contents)));
          if (callback) {
              callback(target);
          }
      }));

}

function stringSrc(filename, string) {
    var src = require('stream').Readable({ objectMode: true });
    src._read = function () {
        this.push(new gutil.File({ cwd: '', base: '', path: filename, contents: new Buffer(string) }));
        this.push(null);
    };
    return src;
}

function cleanKey(obj, key, sep, tag) {
    var croppedKey = key.split(sep).slice(1, (key.split(sep).length) - 1).join(sep).replace(/\W/g, '_');
    var newKey = (tag) ? croppedKey + tag : croppedKey;
    if (obj.hasOwnProperty(newKey)) {
        tag = tag || 0;
        cleanKey(obj, key, sep, parseInt(tag) + 1);
    } else {
        var updatedFile = fs.readFileSync(CLEAN_CONFIG_JS_FULL_PATH, 'utf8').replaceAll(key, newKey);
        fs.writeFileSync (CLEAN_CONFIG_JS_FULL_PATH, updatedFile, 'utf8');
    }
}

function cleanObjectKeys(obj, sep, prefix) {
    for (var key in obj) {
        if (key.startsWith(prefix)) {
            cleanKey(obj, key, sep);
        }
    }
}

function cleanTreehouseConf(obj) {
    cleanObjectKeys(obj.segments, '_', 'video_');
    cleanObjectKeys(obj.nodes, '_', 'node_');
    var updatedFile = fs.readFileSync(CLEAN_CONFIG_JS_FULL_PATH, 'utf8').replaceAll('source\": \"', 'source\": \"ivds.');
    fs.writeFileSync (CLEAN_CONFIG_JS_FULL_PATH, updatedFile, 'utf8');
}

function prepareNodes(nodes) {
    for (var nodeId in nodes) {
        if (nodes.hasOwnProperty(nodeId)) {
            nodes[nodeId].id = nodeId;
            var defaultNodes = [];
            var prefetch = [];
            var seekForwardTo = [];
            for (var i = 0 ; i < nodes[nodeId].channels.length; i++) {
                prefetch.push(nodes[nodeId].channels[i].children);
                seekForwardTo.push(nodes[nodeId].channels[i].seekForwardTo);
                defaultNodes.push(nodes[nodeId].channels[i].defaultNodes);
            }
            nodes[nodeId].data = {
                decision: {},
                seekForwardTo: _.union.apply(null, seekForwardTo),
                defaultNodes: _.union.apply(null, defaultNodes)
            };
            nodes[nodeId].prefetch = _.union.apply(null, prefetch);
            _.extend(nodes[nodeId].data.decision, nodes[nodeId].decision);
            delete nodes[nodeId].channels;
            delete nodes[nodeId].metadata;
            delete nodes[nodeId].decision;
        }
    }
    return nodes;
}

function prepareNodesJs(nodes) {
    return 'define([\'resources/ivds\'],' +
        '\tfunction(ivds) {\n' +
        '\t\treturn ' + JSON.stringify(nodes) +
        ';\n);';
}

gulp.task('test_task', false, ['backup-config'], function() {
    var updatedFile = fs.readFileSync(CLEAN_CONFIG_JS, 'utf8');
    cleanTreehouseConf(JSON.parse(updatedFile));
});

gulp.task('create-project-settings-object', false, function() {
    return parseJson(dirs.src + '/config/settings.json', projSettings, function(target) {
        if (target.embedUrl === '') {
            target.embedUrl = 'https://' + s3Config.bucket + '/' + getFolderName() + '/embed.html';
        }
        if (target.ogUrl === '') {
            target.ogUrl = 'https://' + s3Config.bucket + '/' + getFolderName();
        }
        target.oembedJSON = 'https://' + s3Config.bucket + '/' + getFolderName() + '/oembed.json';
        target.oembedXML = 'https://' + s3Config.bucket + '/' + getFolderName() + '/oembed.xml';
        target.dateAndTime = calcDateAndTime();
        target.iosTitle = fixedEncodeURIComponent(target.creatorName + '' + '"' + target.projectName + '"');
    });
});

gulp.task('create-ivd-folder', 'takes a config.js (treehouse output) file and splits it to ivd files', ['create-treehouse-config-object'], function() {
    mkdirSync(path.join(dirs.import, 'resources'));
    mkdirSync(path.join(dirs.import, 'resources', 'ivd'));
    for (var ivd in treehouseConf.segments) {
        if (treehouseConf.segments.hasOwnProperty(ivd)) {
            stringSrc(ivd + '.ivd', JSON.stringify(treehouseConf.segments[ivd]))
            .pipe(plugins.streamify(plugins.jsonFmt(plugins.jsonFmt.PRETTY)))
            .pipe(gulp.dest(path.join('import', 'resources', 'ivd')));
        }
    }
});

gulp.task('create-nodes:js', 'takes a config.js file (treehouse output) and creates a nodes.js file from it', ['create-treehouse-config-object'], function() {
    mkdirSync(path.join(dirs.import, 'resources'));
    var nodes = prepareNodes(treehouseConf.nodes);
    var str = prepareNodesJs(nodes);
    return stringSrc('nodes.js', str)
    .pipe(plugins.streamify(plugins.jsbeautifier({
        preserveNewlines: false
    })))
    .pipe(gulp.dest(path.join('import', 'resources')));
});


gulp.task('backup-config', false, function() {
    return gulp.src(dirs.import + '/new_config.js')
    .pipe(plugins.rename(CLEAN_CONFIG_JS))
    .pipe(gulp.dest(dirs.import));
});

gulp.task('initial-parse', false, ['backup-config'], function() {
    var updatedFile = fs.readFileSync(CLEAN_CONFIG_JS_FULL_PATH, 'utf8');
    cleanTreehouseConf(JSON.parse(updatedFile));
    return;
});

gulp.task('create-treehouse-config-object', false, ['initial-parse'], function() {
    return parseJson(CLEAN_CONFIG_JS_FULL_PATH, treehouseConf, function() {
        // console.log(treehouseConf);
    });
});

gulp.task('copy:index.html', false, function() {
    return gulp.src('templates/index.html')
    .pipe(plugins.handlebars(projSettings))
    .pipe(gulp.dest(dirs.dist + '/'));
});

gulp.task('copy:embed.html', false, function() {
    return gulp.src('templates/embed.html')
    .pipe(plugins.handlebars(projSettings))
    .pipe(gulp.dest(dirs.dist + '/'));
});

gulp.task('copy:oembed.json', false, function() {
    return gulp.src('templates/oembed.json')
    .pipe(plugins.handlebars(projSettings))
    .pipe(gulp.dest(dirs.dist + '/'));
});

gulp.task('copy:oembed.xml', false, function() {
    return gulp.src('templates/oembed.xml')
    .pipe(plugins.handlebars(projSettings))
    .pipe(gulp.dest(dirs.dist + '/'));
});

gulp.task('copy:publicObject.json', false, function() {
    return gulp.src('templates/publicObject.json')
    .pipe(plugins.handlebars(projSettings))
    .pipe(gulp.dest(dirs.dist + '/'));
});

gulp.task('copy:loader.js', false, function() {
    return gulp.src(dirs.src + '/loader.js')
    .pipe(plugins.handlebars(projSettings))
    .pipe(gulp.dest(dirs.dist + '/'));
});

gulp.task('copy:debug.html', false, function() {
    return gulp.src('templates/debug.html')
    .pipe(gulp.dest(dirs.dist + '/'));
});


function notInArchive(Obj) {
    var ret = Obj.Key.lastIndexOf(getFolderName() + '/archive', 0) === 0;
    return !ret;
}

function listAllKeys(marker, done) {
    S3.listObjects(
        {
            Bucket: s3Config.bucket,
            Marker: marker,
            Prefix: getFolderName()
        }, function(err, data) {
            var rootKeys = data.Contents.filter(notInArchive);
            allKeys = allKeys.concat(rootKeys);
            if (data.IsTruncated) {
                listAllKeys(data.Contents.slice(-1)[0].Key, done);
            } else {
                // console.log(allKeys);
                done();
            }
        });
}

gulp.task('check-sub-folder-existance', false, function(done) {
    doesFolderExist(function() {
        done();
    });
});

function uploadToS3(source, destinationFolder, isSubfolder) {
    if (isSubfolder && overwritingExistingSubfolder) {
        showError('A sub folder of the name \/archive\/' + projSettings.projectVersion + ' already exists. deploy aborted.');
        process.exit();
    }
    var uploadObj = {
        Bucket: s3Config.bucket,
        ACL: s3Config.acl,
        keyTransform: function(relativeFilename) {
            return destinationFolder + '/' + relativeFilename;
        }
    };
    var splitSource = source[0].split('.');
    if (splitSource[splitSource.length - 2] === 'gz') {
        uploadObj.ContentEncoding = 'gzip';
    }
    return gulp.src(source)
    .pipe(plugins.tap(function(file) {
        if (path.basename(file.path) === 'loader.js') {
            file.contents = new Buffer(file.contents.toString().replace('app.min', 'app.min.gz'));
        }
    }))
    .pipe(s3plugin(uploadObj)).on('end', function() {
        showInfo('Project URL: ' + s3Config.bucket + '/' + destinationFolder + '/index.html');
    });
}

function prepareKeys(keys) {
    var key;
    for (var i = 0; i < keys.length; i++) {
        key = keys[i].Key;
        keysForDeletion.push({ Key: key });
        pathsForInvalidation.push('/' + key);
    }
    pathsForInvalidation.push('/' + getFolderName() + '/');
}

gulp.task('list-root-files', false, function(done) {
    listAllKeys('', function() {
        prepareKeys(allKeys);
        // console.log(keysForDeletion);
        showInfo('Invalidating: ' + pathsForInvalidation);
        done();
    });
});

gulp.task('create-invalidation', false, function(done) {
    if (!cmdOptions.production) {
        showInfo('Not overwriting root. No invalidation required.');
        done();
        return;
    }
    if (pathsForInvalidation.length === 0) {
        showInfo('found no files to invalidate');
        done();
        return;
    }
    var invalidationID = uuid.v1();
    var params = {
        DistributionId: cloudfrontConfig.distributionId,
        InvalidationBatch: {
            CallerReference: invalidationID,
            Paths: {
                Quantity: pathsForInvalidation.length,
                Items: pathsForInvalidation
            }
        }
    };
    cloudfront.createInvalidation(params, function(err, data) { // jshint ignore:line
        if (err) {
            showError('invalidation failed');
            showError(err, err.stack);
            done();
        }
        else {
            // console.log(data);
            done();
        }
    });
});

gulp.task('delete-root-version', false, function(done) {
    if (!cmdOptions.production) {
        showInfo('--production flag not present. Not overwriting root.');
        done();
        return;
    }
    if (keysForDeletion.length === 0) {
        showInfo('found no files to delete on root');
        done();
        return;
    }
    S3.deleteObjects({
        Bucket: s3Config.bucket,
        Delete: {
            Objects: keysForDeletion
        }
    }, function(err, data) { // jshint ignore:line
        if (err) {
            showError(err, err.stack); // an error occurred
            throw 'Couldn\'t delete all files in the root';
        }
        else {
            done();
        }
    });
});

gulp.task('upload-to-subfolder', false, function() {
    return uploadToS3(SOURCE_TO_S3, getSubFolderName(), true);
});

gulp.task('upload-zip-to-subfolder', false, function() {
    return uploadToS3(APP_ZIP_SOURCE_TO_S3, getSubFolderName(), true);
});

gulp.task('upload-to-root', false, function() {
    if (!cmdOptions.production) {
        showInfo('--production flag not present. Not uploading to root.');
        return;
    }
    return uploadToS3(SOURCE_TO_S3, getFolderName());
});

gulp.task('upload-zip-to-root', false, function() {
    if (!cmdOptions.production) {
        showInfo('--production flag not present. Not uploading to root.');
        return;
    }
    return uploadToS3(APP_ZIP_SOURCE_TO_S3, getFolderName());
});

gulp.task('watch', false, function() {
    // More generic way
    if (!cmdOptions.src && !cmdOptions.nowatch) {
        showInfo('watching');
        gulp.watch('src/**/*', ['build']);
    }
    // Maybe more effecient but not sync for the task in the array, and it might miss other files
    // gulp.watch('src/css/main.css', ['copy:main.css']);
    // gulp.watch('src/js/**/*', ['lint:js', 'rjsoptimize', 'compressSkins']);
});

// ---------------------------------------------------------------------
// | Main tasks                                                        |
// ---------------------------------------------------------------------

gulp.task('build', 'build a new version of the project in the \'dist\' folder.', function(done) {
    if (cmdOptions.help) {
        runSequence('help', done);
    } else {
        runSequence(
            'generate-project-id',
            'clean',
            'lint:js',
            'copy',
            'remove-comments',
            'inject-css',
            'inject-assets',
            'pack',
            'tinker-appjs',
            'gzip',
            done);
    }
});

// TODO: if there is an error in lint:js, the whole task is stopped
gulp.task('test', 'run \'build\' and then \'serve\'.', function(done) {
    runSequence(
        'build',
        'serve',
        done);
}, {
    options: {
        'nowatch': 'will disable watch over the src folder (watch is active by default)',
        'livereload': 'will reload the page automatically after a change is made in the src folder (relevant only if watch is active)',
        'debug': 'will open a debug page with the debug plugin and other goodies'
    }
});

gulp.task('serve', 'starts a webserver on the dist directory', function(done) {
    runSequence(
        'start-webserver',
        'watch',
        done);
}, {
    options: {
        'nowatch': 'will disable watch over the src folder (watch is active by default)',
        'livereload': 'will reload the page automatically after a change is made in the src folder (relevant only if watch is active)',
        'debug': 'will open a debug page with the debug plugin and other goodies'
    }
});

gulp.task('deploy', 'build the project, bump the and then upload it to S3. Current upload folder: ' + s3Config.bucket + '/' + projSettings.folderName + '/archive/[versionNumber]/', function(done) {
    runSequence(
        'build',
        'bump',
        'upload-to-s3',
        'cms-deploy',
    done);
}, {
    options: {
        'production': 'upload to the root folder (' + s3Config.bucket + '/' + projSettings.folderName + '/) as well as a subfolder ',
        'cms': 'will add the project to Interlude\'s CMS.'
    }
});

gulp.task('upload-to-s3', 'only uploads to S3. Will not perform build and will not bump the version. Same flags as \'deploy\'', function(done) {
    runSequence(
        'check-sub-folder-existance',
        'clean-root',
        'upload',
        'create-invalidation',
    done);
});

gulp.task('clean-root', false, function(done) {
    if (!cmdOptions.production) {
        done();
        return;
    }
    runSequence(
        'list-root-files',
        'delete-root-version',
    done);
});

gulp.task('upload', false, function(done) {
    runSequence(
        'upload-to-subfolder',
        'upload-zip-to-subfolder',
        'upload-to-root',
        'upload-zip-to-root',
    done);
});

gulp.task('cms-deploy', false, function(done) {
    if (cmdOptions.cms) {
        var origRequire = require;

        require('node-amd-require')({ baseUrl: __dirname });
        var app = require('./dist/js/app.js');
        require = origRequire;  // jshint ignore:line

        var project = {
            name: app.settings.projectName,
            creatorName:  app.settings.creatorName,
            projectId: app.settings.projectId,
            appUrl: pkg['boilerplate-configs'].urlPrefix + getSubFolderName() + '/js/app.js',
            thumbnail: {
                default: app.settings.thumbnailUrl
            }
        };

        request.post({
            method: 'POST',
            uri: pkg['boilerplate-configs'].cmsProjectDeployEndpoint,
            headers:{ 'Content-type': 'application/json' },
            body: JSON.stringify(project)
        },
            function (error, response, body) { // jshint ignore:line
                if (error) {
                    showError('cms-deploy: error');
                    showError('project deployment to CMS DB has failed!!!');
                }
                done();
            });
    } else {
        done();
    }
});

gulp.task('pack', false, function(done) {
    var conf = {

        // optimization options
        generateSourceMaps: true,
        preserveLicenseComments: false,
        optimize: UGLIFY ? 'uglify2' : 'none',


        // the module to compile
        baseUrl: './dist',
        // mainConfigFile : './dist/app.js',
        name: 'app',
        out: './dist/app.min.js',
        exclude: ['interlude', 'text'],

        // the paths
        paths: {
            'interlude': 'empty:',
            'lib': 'empty:',
            'project': 'empty:',
            'text': '../bower_components/text/index',
        },
        wrapShim: false
    };

    rjs.optimize(conf, function() { done(); }, done);
});

gulp.task('gzip', false, function() {
    return gulp.src(dirs.dist + '/app.min.js')
    .pipe(plugins.gzip())
    .pipe(plugins.rename('app.min.gz.js'))
    .pipe(gulp.dest(dirs.dist + '/'));
});


gulp.task('import', false, function(done) {
    var url = cmdOptions.sourceUrl || (pkg['boilerplate-configs'].sourceUrl);
    if (!url) {
        done('url parameter must be set in package.json or via command line');
    } else {
        request({
            url: url,
            json: true
        }, function(error, response, body) {
            if (error || response.statusCode !== 200) {
                done(error || response.statusCode);
            } else {
                request(body.config.html5.resources.skins)
                    .pipe(source('skins.json'))
                    .pipe(plugins.replace('\\/', '/'))
                    .pipe(plugins.streamify(plugins.jsonFmt(plugins.jsonFmt.PRETTY)))
                    .pipe(gulp.dest(dirs.import));

                request(body.config.html5.resources.overlays)
                    .pipe(source('overlays.js'))
                    .pipe(plugins.streamify(plugins.jsbeautifier({
                        preserveNewlines: false
                    })))
                    .pipe(gulp.dest(dirs.import));

                request(body.config.html5.url)
                    .pipe(source('config.js'))
                    .pipe(plugins.streamify(plugins.jsbeautifier({
                        preserveNewlines: false
                    })))
                    .pipe(gulp.dest(dirs.import));
            }
        });
    }
});

// TODO: this hard-codes quiz.html which is clearly not general
gulp.task('inject-css', false, function() {
    var extraCssPath = dirs.src + '/stylesheets/juice.css';
    if (fs.existsSync(extraCssPath)) {
        var extraCss = fs.readFileSync(extraCssPath, 'utf8');
        return gulp.src('templates/*.html')
            .pipe(plugins.juice({
                extraCss: extraCss
            }))
            .pipe(plugins.replace(/<((rect|path|line|circle).*)>/g, '<$1/>')) // fix self closing tags
            .pipe(plugins.replace(/[\w\/\.]+(?=\?base64)/g, urlToBase64)) // encode images to base64
            .pipe(plugins.replace(/\?base64/g, '')) // remove redundant placeholder
            .pipe(gulp.dest(dirs.dist + '/templates/'));
    }
});

gulp.task('inject-assets', false, function() {
    return gulp.src(dirs.src + '/js/assets.json')
        .pipe(plugins.replace(/[\w\/\.]+(?=\?base64)/g, urlToBase64))  // encode assets to base64
        .pipe(plugins.replace(/\?base64/g, '')) // remove redundant placeholder
        .pipe(gulp.dest(dirs.dist + '/js/'));
});

gulp.task('tinker-appjs', false, function() {
    return gulp.src(dirs.dist + '/app.min.js')
        .pipe(plugins.replace(/"text!(\.+\/)*/g, '"'))
        .pipe(plugins.replace(/'text!(\.+\/)*/g, '\''))
        .pipe(plugins.replace(/define\("app",/g, 'define('))
        .pipe(plugins.replace(/define\('app',/g, 'define('))
        .pipe(gulp.dest(dirs.dist));
});

gulp.task('require-skins', 'creates a skins.js file in the resources folder that loads all the skin files via requirejs. Skins must be located in src/resource/skins.', ['read-skins'], function(done) {
    var wrapStart = 'define([';
    var deps = '';
    var skinsArray = [];
    for (var filename in resourcesObj) {
        if (resourcesObj.hasOwnProperty(filename)) {
            deps = deps + '\t\'resources/skins/' + filename + '\',\n';
            skinsArray.push(filename);
        }
    }

    var skinsObjectString = '';

    skinsArray.forEach(function(s) {
        skinsObjectString = skinsObjectString + '\t\t' + s + ': ' + s + ',\n';
    });

    deps = deps.slice(0, -2);
    deps = deps.slice(1);
    skinsObjectString = skinsObjectString.slice(0, -2);

    var wrapEnd = '],\nfunction(' + skinsArray + ') {\n' +
        '    var skins = {\n' +
        skinsObjectString +
        '    \n\t};\n' +
        '    return skins;\n' +
        '});\n';

    var data = wrapStart + deps + wrapEnd;

    fs.writeFileSync(REQUIRE_BASE_URL + '/' + SKINS_FILE, data, 'utf8');
    done();
});

gulp.task('require-ivds', 'creates a ivds.js file in the resources folder that loads all the ivd files via requirejs. IVDs must be located in src/resource/ivd.', ['read-ivds'], function(done) {
    var wrapStart = 'define([';
    var deps = '';
    var ivdsArray = [];
    for (var filename in resourcesObj) {
        if (resourcesObj.hasOwnProperty(filename)) {
            deps += '\t\'text!resources/ivd/' + filename + '.ivd\',\n';
            ivdsArray.push('vid_' + filename);
        }
    }

    var ivdsObjectString = '';

    ivdsArray = ivdsArray.map(function(s) {
        return s.replace(/\//g, '_');
    });

    ivdsArray.forEach(function(s) {
        ivdsObjectString += '\t\t' + s + ': ' + s + ',\n';
    });

    deps = deps.slice(0, -2);
    deps = deps.slice(1);
    ivdsObjectString = ivdsObjectString.slice(0, -2);

    var wrapEnd = '],\nfunction(' + ivdsArray.join(', ') + ') {\n' +
        '    var ivds = {\n' +
        ivdsObjectString +
        '    \n\t};\n' +
        '    return ivds;\n' +
        '});\n';

    var data = wrapStart + deps + wrapEnd;

    fs.writeFileSync(REQUIRE_BASE_URL + '/' + IVDS_FILE, data, 'utf8');
    done();
});

gulp.task('read-ivds', false, ['clean-ivds'], function() {
    resourcesObj = {};
    return gulp.src(REQUIRE_BASE_URL + '/' + IVDS_PATH + '/**/*.ivd')
        .pipe(map(createIvdDepObj));

});

gulp.task('read-skins', false, ['clean-skins'], function() {
    resourcesObj = {};
    return gulp.src(REQUIRE_BASE_URL + '/' + SKINS_PATH + '/*.js')
        .pipe(map(createSkinsDepObj));

});

gulp.task('clean-ivds', false, [], function(done) {
    del([
        IVDS_FILE,
        IVDS_INDEX_FILE
    ], done);
});

gulp.task('clean-skins', false, [], function(done) {
    del([
        SKINS_FILE,
        SKINS_INDEX_FILE
    ], done);
});

gulp.task('create-nodes', 'creates a nodes.js files based on all the ivd files in src/resources/ivd. Note: this will overwrite ivds.js and nodes.js in src/resources.', ['require-ivds'], function(cb) {
    var wrapStart = 'define([\n\t\'resources/ivds\'\n' +
        '\t], function(ivds) {\n' +
        '\t\tvar nodes = [\n';
    var wrapEnd = '\treturn nodes;\n' +
        '\t}\n);';
    var nodes = '';
    for (var filename in resourcesObj) {
        if (resourcesObj.hasOwnProperty(filename)) {
            nodes = nodes + '\t\t\t{ id:\'' + filename + '\', source:ivds.vid_' + filename + ' },\n';
        }
    }
    nodes = nodes.slice(0, -2) + '];\n';

    var data = wrapStart + nodes + wrapEnd;

    fs.writeFileSync(dirs.src + '/' + NODES_FILE, data, 'utf8', function(err) {
        if (err) {
            cb(err);
        }
        cb();
    });

});

gulp.task('default', ['build']);
